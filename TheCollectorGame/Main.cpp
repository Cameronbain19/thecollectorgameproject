
// Including libraries
#include <SFML/Graphics.hpp> // Library required for creating and managing SFML windows

// Library needed for playing music and sound effects
#include <SFML/Audio.hpp>

//Library for manipulating string of text
#include <string>

#include <cstdlib>

#include <time.h>;

// include our own class definitions
#include "Player.h"
#include "Item.h"

// Main function
int main()
{
	//Declare our SFML window passing the dimensions and the window name
    sf::RenderWindow gameWindow;

	gameWindow.create(sf::VideoMode(800, 600), "The collector", sf::Style::Titlebar | sf::Style::Close);
	
	//------------------------ Game Setup ---------------------//

	// Player 
	//Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	Player playerObject(playerTexture, gameWindow.getSize());

	//Game music
	// Declare a music variable called gameMusic
	sf::Music gameMusic;

	//Load up our audio from a file path
	gameMusic.openFromFile("Assets/Audio/music.ogg");

	// Start our music playing
	gameMusic.play();

	// Game Font
	//Declare a font variable called gameFont
	sf::Font gameFont;

	//Load up the font from a file path
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	// Title Text
	// Declare a text variable called titleText to hold our game title display
	sf::Text titleText;

	// Set the font our text should use
	titleText.setFont(gameFont);

	//Set the string of text that will be displayed by this text object
	titleText.setString("The collector");

	// Set the size of our text, in pixels
	titleText.setCharacterSize(24);

	// Set the colour our our text
	titleText.setFillColor(sf::Color::Cyan);

	// Set the text style for the text
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);

	// Position our text in the top center of the screen
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);

	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("by Cameron Bain");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Cyan);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);

	// Score
	//Declare an integer variable to hold our numerical score value
	int score = 0;

	// Setup score text object
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	// Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);

	// Create a time value to score the total time limit for our game
	sf::Time timeLimit = sf::seconds(60.0f);

	// Create a timer to store the time remaining for our game
	sf::Time timeRemaining = timeLimit;

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;


	srand(time(NULL));

	// Items
	// Load all three textures that will be used by our items
	std::vector<sf::Texture> itemTextures;
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures[0].loadFromFile("Assets/Graphics/coinBronze.png");
	itemTextures[1].loadFromFile("Assets/Graphics/coinSilver.png");
	itemTextures[2].loadFromFile("Assets/Graphics/coinGold.png");

	// Create the vector to hold our items
	std::vector<Item> items;

	// Load up a few random starting items 
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));

	// Create a time value to store the total time between each item spawn
	sf::Time itemSpawnDuration = sf::seconds(2.0f);

	// Create a timer to store the time remaining for our game
	sf::Time itemSpawnRemaining = itemSpawnDuration;

	sf::Vector2f playerVelocity(0.0f, 0.0f);

	float speed = 100.0f;

	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");

	// Setup a sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	// Load victory sound effect file into a soundBuffer
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");

	// Setup a sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound VictorySound;
	VictorySound.setBuffer(victorySoundBuffer);

	// Game over variable to track if the game is done
	bool gameOver = false;

	// Game over text
	// Declare a text variable called gameOverText to hold our game over display
	sf::Text gameOverText;

	// Set the font oue text should use 
	gameOverText.setFont(gameFont);

	// Set the string of text that will be displayed by this text object
	gameOverText.setString("GAME OVER");

	// Set the size of our text, in pixels
	gameOverText.setCharacterSize(72);

	// Set the colour for the text
	gameOverText.setFillColor(sf::Color::Cyan);

	// Set the text style for the text
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);

	// Position our text in the top center of the screen
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);


	// Game Loop
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// ---------- Inputs ------------// 

		//Declare a variable ot hold an Event, called gameEvent
		sf::Event gameEvent;

		//Loop through all events and poll them, putting each one intop our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			//This section will be repates for each event waiting to be processed

			//Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{

				//if so, call the close function on the window
				gameWindow.close();

			}


		}
		// End event polling loop

		// Player keybind input
		playerObject.Input();

		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			// Reset the game
			score = 0;
			timeRemaining = timeLimit;
			items.clear();
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			gameMusic.play();
			gameOver = false;
			playerObject.Reset(gameWindow.getSize());
		}


		//----------------- Update Section ------------//

		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();

		if (!gameOver)
		{
			// Update our item spawn time remaining based on how much time passed last frame
			itemSpawnRemaining = itemSpawnRemaining - frameTime;

			// Check if time remaining to next spawn has reached 0
			if (itemSpawnRemaining <= sf::seconds(0.0f))
			{
				// Time to spawn a new item
				items.push_back(Item(itemTextures, gameWindow.getSize()));

				// Reset time remaining to full duration
				itemSpawnRemaining = itemSpawnDuration;
			}
		}
		// Update our time remaining based on how much time passed last frame
		timeRemaining = timeRemaining - frameTime;

		// Check if time has run out
		if (timeRemaining.asSeconds() <= 0)
		{
			// Don't let the time go lower than 0
			timeRemaining = sf::seconds(0);

			// Perform these actions only once when the game first ends:
			if (gameOver == false)
			{
				// Set our gameOver to true now so we don't perform these actions again
				gameOver = true;

				// Stop the main music from playing
				gameMusic.stop();


				// Play our victory sound
				VictorySound.play();
			}
		}

		//Update our time display based on our time remaining
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));



		scoreText.setString("Score: " + std::to_string(score));

		// Move the player
		playerObject.Update(frameTime);

		// Check for collisions
		for (int i = items.size() - 1; i >= 00; --i)
		{
			sf::FloatRect itemBounds = items[i].sprite.getGlobalBounds();
			sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();

			if (itemBounds.intersects(playerBounds))
			{
				// Our player touched the item
				// Add the item's value to the score
				score += items[i].pointsValue;

				// Remove the item from the vector
				items.erase(items.begin() + i);

				// Play the pickup sound
				pickupSound.play();
			}
		}


		//----------------- Draw Section -------------//

		// Clear the window to a single colour
		gameWindow.clear(sf::Color::Black);

		// Draw everything to the window
		gameWindow.draw(titleText);
		gameWindow.draw(authorText);
		gameWindow.draw(scoreText);
		gameWindow.draw(timerText);

		// Only draw these items if the game had NOT ended:
		if (!gameOver)
		{
			gameWindow.draw(playerObject.sprite);

			// Draw all our items
			for (int i = 0; i < items.size(); ++i)
			{
				gameWindow.draw(items[i].sprite);
			}

		}

		if (gameOver) 
		{
			gameWindow.draw(gameOverText);
		}

		//Display the window contents on the screen
		gameWindow.display();

	}
	// End of Game Loop

	return 0;
}
// End of Main Function