#pragma once

// Library needed for using sprites, textures and fonts
#include <SFML/Graphics.hpp>

// Library for handling collections of objects
#include<vector>

// Definition of Item class
class Item
{
    // access level
    public:
     
    // Constructor
    Item(std::vector<sf::Texture> & itemTextures, sf::Vector2u screenSize);

    // Variables used by this class
	sf::Sprite sprite;
	int pointsValue;

};

