#include "Player.h"

Player::Player(sf::Texture& playerTexture, sf::Vector2u screensize)
{
	sprite.setTexture(playerTexture);

	sprite.setPosition(screensize.x / 2 - playerTexture.getSize().x / 2, screensize.y / 2 - playerTexture.getSize().y / 2);

	velocity.x = 0.0f;
	velocity.y = 0.0f;

	speed = 100.0f;
}

void Player::Input()
{
	velocity.x = 0.0f;
	velocity.y = 0.0f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}
	
}

void Player::Update(sf::Time frameTime)
{
	// Move the player
	sprite.setPosition(sprite.getPosition() + velocity * frameTime.asSeconds());
}

void Player::Reset(sf::Vector2u screenSize)
{
	sprite.setPosition(screenSize.x / 2 - sprite.getTexture()->getSize().x / 2, screenSize.y / 2 - sprite.getTexture()->getSize().y / 2);

}

