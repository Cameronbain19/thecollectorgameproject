#pragma once

// Library needed for using sprites, texures and fonts
#include <SFML/Graphics.hpp>

// Library for handling collections of objects
#include <vector>

class Player
{
	// Access level
    public:

	// Constructor
		Player(sf::Texture& playerTexture, sf::Vector2u screensize);

		// Functions to call Player-specific code
		void Input();
		void Update(sf::Time frameTime);
		void Reset(sf::Vector2u screenSize);

		// Variables used by this class
		sf::Sprite sprite;
		sf::Vector2f velocity;
		float speed;
};

